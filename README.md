# Loreal Budget Allocation Shift Analysis #

In this repository you can find all scripts, data and explorative Analysis
regarding the Loreal Budget Allocation analysis.

### What was the goal of the analysis ###

Loreal wants to allocate the marketing budget differently
Instead of allocating most of the budget on tv a shift to the online channels
(Youtube, Search, Online) is being discussed.

This repo is analysing the effects that this shift in Budget would
have on the target value (revenue) by playing trough different scenarios of
budget distribution.

### Where is the data, scripts and anaysis being stored? ###

* /data contains all raw and processed data  
* /scripts contains all scripts for the cleaning, modeling and predicting  
* /analysis_and_graphs contains all excel and jpeg analysis
* requirements.txt contains all the python packages that are needed for this project
  -> install on your local machine with "pip install -r requirements.txt"


### Repo Owner & Repsonsible ###

* Charlotte Veitner (charlotte_friederike.veitner@seven.one)
