# importing general models
import os
import sys

# importing desired packages for python
import pandas as pd
import numpy as np
import warnings
from datetime import timedelta, datetime
import holidays
import matplotlib.pyplot as plt
import fbprophet
import math
import plotly.graph_objects as go

CWD = os.path.abspath('.')
sys.path.append(CWD + '/scripts')

try:
    import helper_functions as helper
except:
    print('trying different syntax for windows')
    try:
        sys.path.append(CWD + '\\scripts')
        import helper_functions as helper
    except:
        print('there was an error importing the submodule')



# display settings for output
pd.options.display.max_columns = 50
pd.options.display.max_rows = 2000

# Filter the warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
df = pd.read_csv(CWD + '/data/johnson_raw.csv')

cap = df.Umsatz.max()*1.2
# Create testing and training dataframe using 2020-03-01 as the split date
df_training, df_testing, df_all, df_holidays = helper.process_dataframe_and_make_split(df, cap=cap, floor=100000, netto_percent_tv=0.15, cut_off_date='2020-01-01')

columns_to_keep_training = {
                            'ds':'ds',
                            'Umsatz': 'y',
                            'cap':'cap',
                            'floor':'florr',
                            'TV' :'tv_spendings',
                            'Online': 'online_spendings',
                            'YouTube': 'youtube_spendings',
                            'Search': 'search_spendings',
                            'Google_Trend_Index': 'google_trend_index',
                            'Total_Spend': 'sum_all_spendings',
                            'perc_tv':'perc_tv',
                            'perc_online':'perc_online',
                            'sum_all_online':'sum_all_online',
                           }
df_training = helper.subset_and_rename(df_training, columns_to_keep_training)

"""DATA HARD TO MODEL"""

df_all.plot(x='ds', y='Umsatz', kind='line')
fig_real = go.Figure()

fig_real.add_trace(go.Scatter(x=df_all['ds'], y=df_all['Umsatz'],
                        name='Umsatz',
                        line=dict(color='firebrick', width=4)
                        ))
fig_real.update_layout(title='Revenue development Johnson',
                           xaxis_title='Date',
                           yaxis_title='Revenue (in €)')
# plt.show()
# create testing sets with different budget allocation
columns_to_keep_testing = {
                            'ds':'ds',
                            'Umsatz': 'y',
                            'cap':'cap',
                            'floor':'florr',
                            'TV' :'tv_spendings',
                            'Online': 'online_spendings',
                            'YouTube': 'youtube_spendings',
                            'Search': 'search_spendings',
                            'Google_Trend_Index': 'google_trend_index',
                            'percent_online': 'percent_online',
                            'percent_youtube': 'percent_youtube',
                            'percent_search': 'percent_search',
                            'sum_all_spendings': 'sum_all_spendings',
                            'perc_tv':'perc_tv',
                            'perc_online':'perc_online',
                            'sum_all_online':'sum_all_online'
                          }
# Build dataframe with renamed columns for TV allocation as it is
df_testing_tv100 = helper.subset_and_rename(df_testing, columns_to_keep_testing)

model = fbprophet.Prophet(
                            growth='logistic',
                            # manual adding changepoints
                            #changepoints= maybe add corona as list of changepoints
                            n_changepoints=10,
                            #increase for more flexibility
                            changepoint_prior_scale=0.4,
                            # Fouriers Order for seasonalities
                            yearly_seasonality=False,
                            weekly_seasonality=False,
                            daily_seasonality=False,
                            holidays=df_holidays,
                            seasonality_mode='additive',
                            holidays_prior_scale=0.5,
                            interval_width=0.8
                        )
# add monthly seasonality
model.add_seasonality(name='yearly', period=365, fourier_order=20, prior_scale=0.33)
model.add_seasonality(name='monthly', period=52/12, fourier_order=5, prior_scale=0.4)
model.add_regressor('tv_spendings', prior_scale=0.32, mode='multiplicative')

model.add_regressor('perc_tv', prior_scale=0.2, mode='additive')
model.add_regressor('perc_online', prior_scale=0.2, mode='additive')
model.add_regressor('sum_all_online', prior_scale=0.2, mode='additive')

# model.add_regressor('online_spendings', prior_scale=0.6, mode='additive')
# model.add_regressor('youtube_spendings', prior_scale=0.6, mode='additive')
# model.add_regressor('search_spendings', prior_scale=0.6, mode='additive')
#model.add_regressor('google_trend_index', prior_scale=0.02, mode='additive')
model.fit(df_training)
#df_testing = df_testing_tv100.copy()
# help(fbprophet.Prophet)
# help(model.add_regressor)
#help(model.add_seasonality)

eval_df_100, mae_100, rmse_100, mape_100, monthly_mape_100 = helper.predict_and_evaluate(df_testing_tv100, model)
print('the mape for the original budget allocation is {}'.format(mape_100))
print('the monhtly aggregate mape for the original budget allocation is {}'.format(monthly_mape_100))
eval_df_100['mape'] = mape_100*100



def see_components_of_prediction(df_testing, model):
    df_testing = df_testing_tv100.copy()
    X_test = df_testing.copy().drop(columns=['y'])
    y_test = df_testing[['ds','y']].rename(columns={'y':'y_real'})
    y_predict = model.predict(X_test)
    fig = model.plot(y_predict)

y_pred_100 = model.predict(df_testing_tv100.drop(columns=['y']))
y_compare_100 = helper.subset_and_rename(y_pred_100, {'ds':'ds', 'yhat':'y_pred_100'}).round(2)
y_compare_real = helper.subset_and_rename(df_testing_tv100,{'ds':'ds','y':'y_real'}).round(2)


fig = model.plot(y_pred_100)
y_pred_all_scenarios = y_compare_real.merge(y_compare_100)

from plotly.subplots import make_subplots

# multiple line plot
fig_validation = make_subplots(specs=[[{"secondary_y": True}]])
fig_validation.add_trace(go.Scatter(x=eval_df_100['ds'], y=eval_df_100['y_real'],
                            name='Tatsächlicher Umsatz',
                            line=dict(color='#FF0230', width=3)
                            ))
fig_validation.add_trace(go.Scatter(x=eval_df_100['ds'], y=eval_df_100['y_predict'],
                            name='Vorhergesagter Umsatz',
                            line=dict(color='#217363', width=4, dash='dot')),
                            secondary_y=False
                            )
# fig_validation.add_trace(go.Scatter(x=eval_df_100['ds'], y=eval_df_100['mape'],
#                             name='MAPE',
#                             line=dict(color='#00FFCF', width=2)),
#                             secondary_y=True
#                             )

fig_validation.update_layout(title='Vorhersage für Umsatz bei gleicher Budgetverteilung - Johnson & Johnson',
                               xaxis=dict(
                                    title_text='Datum',
                                    title_font = {"size": 20},
                                ticks='outside',
                                tickfont=dict(
                                    family='Montserrat',
                                    size=12,
                                    color='black',
                                ),),
                               #yaxis_title='percent diff [%])',
                               plot_bgcolor="#F2F2F2",
                               width=1100,
                               height=600,
                               font=dict(
                                    family="Montserrat",
                                    size=19,
                                    color="black"
                                    )
                            )

fig_validation.update_yaxes(
                                title_text='Umsatz [€]',
                                #range=[-40,10],
                                title_font = {"size": 20},
                                ticks='outside',
                                tickfont=dict(
                                                family='Montserrat',
                                                size=12,
                                                color='black',
                                             ),
                                secondary_y=False)


# fig_validation.update_yaxes(
#                                 title_text='MAPE [%]',
#                                 range=[0,100],
#                                 title_font = {"size": 20},
#                                 ticks='outside',
#                                 tickfont=dict(
#                                                 family='Montserrat',
#                                                 size=12,
#                                                 color='black',
#                                              ),
#                                 secondary_y=True)

# multiple line plot
fig = go.Figure()
fig.add_trace(go.Scatter(x=y_pred_all_scenarios['ds'], y=y_pred_all_scenarios['y_real'],
                    name='Real Values',
                    line=dict(color='firebrick', width=4)
                    ))
fig.add_trace(go.Scatter(x=y_pred_all_scenarios['ds'], y=y_pred_all_scenarios['y_pred_100'],
                    name='TV 100%',
                    line=dict(color='#ff944d', width=3, dash='dash')
                    ))
fig.update_layout(title='Prediction for Revenue (for period where TV was switched to 0%)',
                   xaxis_title='Date',
                   yaxis_title='Revenue (in €)')
