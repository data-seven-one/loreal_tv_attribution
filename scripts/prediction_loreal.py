# importing general models
import os
import sys

# importing desired packages for python
import pandas as pd
import numpy as np
import warnings
from datetime import timedelta, datetime
import holidays
import matplotlib.pyplot as plt
import fbprophet
import math
import plotly.graph_objects as go
print('this is just a test')
# global settings
CWD = os.path.abspath('.')
sys.path.append(CWD + '/scripts')

try:
    import helper_functions as helper
except:
    print('trying different syntax for windows')
    try:
        sys.path.append(CWD + '\\scripts')
        import helper_functions as helper
    except:
        print('there was an error importing the submodule')



# display settings for output
pd.options.display.max_columns = 50
pd.options.display.max_rows = 2000

# Filter the warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
df = pd.read_csv(CWD + '/data/loreal_raw.csv')
cap = df.Umsatz.max()*1.1
floor = df.Umsatz.min()



# Create testing and training dataframe using 2020-03-01 as the split date
df_training, df_testing, df_all, df_holidays = helper.process_dataframe_and_make_split(df, cap=cap, floor=100000, netto_percent_tv=0.15, cut_off_date='2020-03-01')

columns_to_keep_training = {
                            'ds':'ds',
                            'Umsatz': 'y',
                            'cap':'cap',
                            'floor':'florr',
                            'TV' :'tv_spendings',
                            'Online': 'online_spendings',
                            'YouTube': 'youtube_spendings',
                            'Search': 'search_spendings',
                            'Google_Trend_Index': 'google_trend_index',
                            'Total_Spend': 'sum_all_spendings',
                            'perc_tv':'perc_tv',
                            'perc_online':'perc_online',
                            'sum_all_online':'sum_all_online',
                           }
df_training = helper.subset_and_rename(df_training, columns_to_keep_training)
# df_training.plot(x='ds', y='y', kind='line')
# plt.show()
# create testing sets with different budget allocation
columns_to_keep_testing = {
                            'ds':'ds',
                            'Umsatz': 'y',
                            'cap':'cap',
                            'floor':'florr',
                            'TV' :'tv_spendings',
                            'Online': 'online_spendings',
                            'YouTube': 'youtube_spendings',
                            'Search': 'search_spendings',
                            'Google_Trend_Index': 'google_trend_index',
                            'percent_online': 'percent_online',
                            'percent_youtube': 'percent_youtube',
                            'percent_search': 'percent_search',
                            'sum_all_spendings': 'sum_all_spendings',
                            'perc_tv':'perc_tv',
                            'perc_online':'perc_online',
                            'sum_all_online':'sum_all_online'
                          }
# Build dataframe with renamed columns for TV allocation as it is
df_testing_tv100 = helper.subset_and_rename(df_testing, columns_to_keep_testing)
# df_testing_tv100.plot(x='ds', y='y', kind='line')
# plt.show()

# Spread 25%, 50%, 75%, 100% of the budget across the other online channels (assuming that the distribution across
# those channels remains the same)
df_testing_tv75 = helper.subset_and_rename(df_testing, columns_to_keep_testing)
df_testing_tv75 = helper.distribute_budget_evenly(df_testing_tv75, 0.75)

df_testing_tv50 = helper.subset_and_rename(df_testing, columns_to_keep_testing)
df_testing_tv50 = helper.distribute_budget_evenly(df_testing_tv50, 0.5)

df_testing_tv25 = helper.subset_and_rename(df_testing, columns_to_keep_testing)
df_testing_tv25 = helper.distribute_budget_evenly(df_testing_tv25, 0.25)

df_testing_tv00 = helper.subset_and_rename(df_testing, columns_to_keep_testing)
df_testing_tv00 = helper.distribute_budget_evenly(df_testing_tv00, 0)

df_testing = df_testing_tv100.copy()

# Building fpbrophet construct
model = fbprophet.Prophet(
                            growth='logistic',
                            # manual adding changepoints
                            #changepoints= maybe add corona as list of changepoints
                            n_changepoints=10,
                            #increase for more flexibility
                            changepoint_prior_scale=0.4,
                            # Fouriers Order for seasonalities
                            yearly_seasonality=False,
                            weekly_seasonality=False,
                            daily_seasonality=False,
                            holidays=df_holidays,
                            seasonality_mode='additive',
                            holidays_prior_scale=0.5,
                            interval_width=0.8
                        )
# add monthly seasonality
model.add_seasonality(name='yearly', period=365, fourier_order=20, prior_scale=0.33)
model.add_seasonality(name='monthly', period=52/12, fourier_order=5, prior_scale=0.4)
model.add_regressor('tv_spendings', prior_scale=0.032, mode='multiplicative')

model.add_regressor('perc_tv', prior_scale=0.02, mode='additive')
model.add_regressor('perc_online', prior_scale=0.02, mode='additive')
model.add_regressor('sum_all_online', prior_scale=0.02, mode='additive')

# model.add_regressor('online_spendings', prior_scale=0.6, mode='additive')
# model.add_regressor('youtube_spendings', prior_scale=0.6, mode='additive')
# model.add_regressor('search_spendings', prior_scale=0.6, mode='additive')
#model.add_regressor('google_trend_index', prior_scale=0.02, mode='additive')
model.fit(df_training)
#df_testing = df_testing_tv100.copy()
# help(fbprophet.Prophet)
# help(model.add_regressor)
#help(model.add_seasonality)

eval_df_100, mae_100, rmse_100, mape_100, monthly_mape_100 = helper.predict_and_evaluate(df_testing_tv100, model)
print('the mape for the original budget allocation is {}'.format(mape_100))
print('the monhtly aggregate mape for the original budget allocation is {}'.format(monthly_mape_100))


#def see_components_of_prediction(df_testing, model):

X_test = df_testing.copy().drop(columns=['y'])
y_test = df_testing[['ds','y']].rename(columns={'y':'y_real'})
y_predict = model.predict(X_test)
#
fig = model.plot(y_predict)
#model.plot_components(y_predict)

y_pred_50 = model.predict(df_testing_tv50)

#model.plot_components(y_pred_50)

y_pred_100 = model.predict(df_testing_tv100)
y_pred_75 = model.predict(df_testing_tv75)
y_pred_50 = model.predict(df_testing_tv50)
y_pred_25 = model.predict(df_testing_tv25)
y_pred_00 = model.predict(df_testing_tv00)


y_compare_100 = helper.subset_and_rename(y_pred_100, {'ds':'ds', 'yhat':'y_pred_100'}).round(2)
y_compare_75 = helper.subset_and_rename(y_pred_75, {'ds':'ds', 'yhat':'y_pred_75'}).round(2)
y_compare_50 = helper.subset_and_rename(y_pred_50, {'ds':'ds', 'yhat':'y_pred_50'}).round(2)
y_compare_25 = helper.subset_and_rename(y_pred_25, {'ds':'ds', 'yhat':'y_pred_25'}).round(2)
y_compare_00 = helper.subset_and_rename(y_pred_00, {'ds':'ds', 'yhat':'y_pred_00'}).round(2)
y_compare_real = helper.subset_and_rename(df_testing_tv100,{'ds':'ds','y':'y_real'}).round(2)


y_pred_all_scenarios = y_compare_real.merge(y_compare_100).merge(y_compare_75).merge(y_compare_50).merge(y_compare_25).merge(y_compare_00)

y_pred_all_scenarios['%_diff_revenue_100'] = 100*(-y_pred_all_scenarios['y_pred_100']+y_pred_all_scenarios['y_pred_100'])/y_pred_all_scenarios['y_pred_100']
y_pred_all_scenarios['%_diff_revenue_75'] = 100*(-y_pred_all_scenarios['y_pred_100']+y_pred_all_scenarios['y_pred_75'])/y_pred_all_scenarios['y_pred_100']
y_pred_all_scenarios['%_diff_revenue_50'] = 100*(-y_pred_all_scenarios['y_pred_100']+y_pred_all_scenarios['y_pred_50'])/y_pred_all_scenarios['y_pred_100']
y_pred_all_scenarios['%_diff_revenue_25'] = 100*(-y_pred_all_scenarios['y_pred_100']+y_pred_all_scenarios['y_pred_25'])/y_pred_all_scenarios['y_pred_100']
y_pred_all_scenarios['%_diff_revenue_00'] = 100*(-y_pred_all_scenarios['y_pred_100']+y_pred_all_scenarios['y_pred_00'])/y_pred_all_scenarios['y_pred_100']

# multiple line plot
fig_validation = go.Figure()
fig_validation.add_trace(go.Scatter(x=y_pred_all_scenarios['ds'], y=y_pred_all_scenarios['y_real'],
                            name='Tatsächlicher Umsatz',
                            line=dict(color='#FF0230', width=3)
                            ))
fig_validation.add_trace(go.Scatter(x=y_pred_all_scenarios['ds'], y=y_pred_all_scenarios['y_pred_100'],
                            name='Vorhergesagter Umsatz',
                            line=dict(color='#217363', width=4, dash='dot')
                            ))
fig_validation.update_layout(title="Vorhersage für Umsatz bei gleicher Budgetverteilung - L'Oréal Paris",
                               xaxis=dict(
                                    title_text='Datum',
                                    title_font = {"size": 20},
                                ticks='outside',
                                tickfont=dict(
                                    family='Montserrat',
                                    size=12,
                                    color='black',
                                ),),
                                yaxis=dict(
                                    title_text='Umsatz [€]',
                                    #range=[-40,10],
                                    title_font = {"size": 20},
                                ticks='outside',
                                tickfont=dict(
                                    family='Montserrat',
                                    size=12,
                                    color='black',
                                ),),
                               #yaxis_title='percent diff [%])',
                               plot_bgcolor="#F2F2F2",
                               width=1100,
                               height=600,
                               font=dict(
                                    family="Montserrat",
                                    size=19,
                                    color="black"
                                    )
                            )


# multiple line plot
fig = go.Figure()
fig.add_trace(go.Scatter(x=y_pred_all_scenarios['ds'], y=y_pred_all_scenarios['y_real'],
                    name='Tatsächlicher Umsatz',
                    line=dict(color='#FF0230', width=3)
                    ))
fig.add_trace(go.Scatter(x=y_pred_all_scenarios['ds'], y=y_pred_all_scenarios['y_pred_100'],
                    name='TV 100%',
                    line=dict(color='#00FFCF', width=3, dash='dot')
                    ))
fig.add_trace(go.Scatter(x=y_pred_all_scenarios['ds'], y=y_pred_all_scenarios['y_pred_75'],
                    name='TV 75% & 25% Shift',
                    line=dict(color='#219C87', width=3, dash='dot')
                    ))
fig.add_trace(go.Scatter(x=y_pred_all_scenarios['ds'], y=y_pred_all_scenarios['y_pred_50'],
                    name='TV 50% & 50% Shift',
                    line=dict(color='#217363', width=3, dash='dot')
                    ))
fig.add_trace(go.Scatter(x=y_pred_all_scenarios['ds'], y=y_pred_all_scenarios['y_pred_25'],
                    name='TV 25% & 75% Shift',
                    line=dict(color='#215049', width=3, dash='dot')
                    ))
fig.add_trace(go.Scatter(x=y_pred_all_scenarios['ds'], y=y_pred_all_scenarios['y_pred_00'],
                    name='TV 0% & 100% Shift',
                    line=dict(color='#103A33', width=3, dash='dot')
                    ))

fig.update_layout(title='Umsatzentwicklung für TV-Budget Shifts',
                   xaxis=dict(
                        title_text='Datum',
                        title_font = {"size": 20},
                    ticks='outside',
                    tickfont=dict(
                        family='Montserrat',
                        size=12,
                        color='black',
                    ),),
                    yaxis=dict(
                        title_text='Umsatz [€]',
                        #range=[-40,10],
                        title_font = {"size": 20},
                    ticks='outside',
                    tickfont=dict(
                        family='Montserrat',
                        size=12,
                        color='black',
                    ),),
                   #yaxis_title='percent diff [%])',
                   plot_bgcolor="#F2F2F2",
                   width=1100,
                   height=600,
                   font=dict(
                        family="Montserrat",
                        size=20,
                        color="black"
                        )
                    )


fig_perc = go.Figure()
fig_perc.add_trace(go.Scatter(x=y_pred_all_scenarios['ds'], y=y_pred_all_scenarios['%_diff_revenue_100'],
                    name='TV 100%',
                    line=dict(color='#000000', width=3)
                    ))
fig_perc.add_trace(go.Scatter(x=y_pred_all_scenarios['ds'], y=y_pred_all_scenarios['%_diff_revenue_75'],
                    name='TV 75%',
                    line=dict(color='#00FFCF', width=3, dash='dash')
                    ))
fig_perc.add_trace(go.Scatter(x=y_pred_all_scenarios['ds'], y=y_pred_all_scenarios['%_diff_revenue_50'],
                    name='TV 50%',
                    line=dict(color='#217363', width=3, dash='dash')
                    ))
fig_perc.add_trace(go.Scatter(x=y_pred_all_scenarios['ds'], y=y_pred_all_scenarios['%_diff_revenue_25'],
                    name='TV 25%',
                    line=dict(color='#D80E32', width=3, dash='dash')
                    ))
fig_perc.add_trace(go.Scatter(x=y_pred_all_scenarios['ds'], y=y_pred_all_scenarios['%_diff_revenue_00'],
                    name='TV 0%',
                    line=dict(color='#6F1B34', width=3, dash='dash')
                    ))
fig_perc.update_layout(title='Umsatzdifferenz bei TV-Budget Shifts in Prozent',
                   xaxis=dict(
                        title_text='Datum',
                        title_font = {"size": 20},
                    ticks='outside',
                    tickfont=dict(
                        family='Montserrat',
                        size=12,
                        color='black',
                    ),),
                    yaxis=dict(
                        title_text='Differenz in Prozent [%]',
                        range=[-40,10],
                        title_font = {"size": 20},
                    ticks='outside',
                    tickfont=dict(
                        family='Montserrat',
                        size=12,
                        color='black',
                    ),),
                   #yaxis_title='percent diff [%])',
                   plot_bgcolor="#F2F2F2",
                   width=1100,
                   height=600,
                   font=dict(
                        family="Montserrat",
                        size=20,
                        color="black"
                        )
                    )
#
