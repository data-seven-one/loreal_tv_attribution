# importing general models
import os
import sys

# importing desired packages for python
import pandas as pd
import numpy as np
import warnings
from datetime import timedelta, datetime
import holidays
import matplotlib.pyplot as plt
import fbprophet
import math
# global settings
CWD = os.path.abspath('.')

# display settings for output
pd.options.display.max_columns = 50
pd.options.display.max_rows = 2000



def subset_and_rename(df, dict):
    """ Function to take a subset and rename the columns
    of a DataFrame.

    input: df = Dataframe with undedited columns and naming
           dict = python dictionary to assign new names to the
                  desired columns. All columns that are not listed
                  in the dictionary will be dropped from the DataFrame
    output: df = Dataframe with desired coumns and names

    eg:
    input: df.columns = 'original_name', 'col1', 'col2', 'col3',...
         dict = {'original_name': 'desired_name','col1': 'better_name',....}
    output: df.columns = 'desired_name', 'better_name'
    """

    columns = list(dict.keys())
    df = df[columns]

    df = df.rename(columns=dict)

    return df


def distribute_budget_evenly(df, percent_keep):
    """ Function to distribute the spendings of TV along
    the online channels online, youtube and search (keeping the
    current distribution across those three channels)
    The total spendings across all four channels will remain the same!

    input: df = Dataframe with original budget allocation
           percent_keep = percent of original value to stick with tv
                          (eg. to keep 75% with TV and distribute the
                          remaining 25% -> percent_keep=0.75)
    output: df = Dataframe with changed budget distribution
    """
    # make  a helper column with the amount of budget that is being shifted
    df['to_distribute'] = (1-percent_keep) * df['tv_spendings']
    # subtract the amount to distribute from the original tv spendings
    df['tv_spendings'] = round(df['tv_spendings'] - df['to_distribute'],2)
    # distribute the amount that is being shifted over the three other channels
    # assuming that the reation of those will remain the same
    df['online_spendings'] = round(df['online_spendings'] + (np.multiply(df['to_distribute'],df['percent_online'])) ,2)
    df['youtube_spendings'] = round(df['youtube_spendings'] + (np.multiply(df['to_distribute'],df['percent_youtube'])) ,2)
    df['search_spendings'] = round(df['search_spendings'] + (np.multiply(df['to_distribute'],df['percent_search'])) ,2)
    # create addiitonal columns for all spendings in order to get the
    # percent values for the amount that was spend on tv and online respectively
    df['sum_all_spendings'] = df['tv_spendings'] + df['online_spendings'] + df['youtube_spendings'] + df['search_spendings']
    df['sum_all_online'] = df['online_spendings'] + df['youtube_spendings'] + df['search_spendings']
    df['perc_tv'] = df['tv_spendings']/df['sum_all_spendings']
    df['perc_online'] = df['sum_all_online']/df['sum_all_spendings']
    # drop the helper columns and the columns of the original distribution of the online channels
    df = df.drop(columns=['to_distribute', 'percent_online', 'percent_youtube', 'percent_search'])
    return df


def __make_holiday_df(df):
    """ Function to create a holiday dataframe with all the
    bank holidays in Germany across the period of the given DataFrame

    input: df = Dataframe with all datapoints. This is only needed to
                extract the desired years for the holiday dataframe

    output: df = Dataframe with the columns ds and holiday that
                 can be used directly when building the fbprophet model
    """
    # Extract the years from the origina dataframe to use as input for the holiday function
    years_list = list(df['ds'].apply(lambda x: x.year).drop_duplicates())
    # Make Holiday Dataframe
    # Rename the columns to ds and holiday as it is expected in the fbprophet syntax
    germany_holidays = pd.DataFrame(holidays.Germany(years=years_list).items()).rename(columns={0:'ds',1:'holiday'})
    # Make every datestamp to the Monday of the given week to match the raw data
    germany_holidays['ds'] = germany_holidays['ds'].apply(lambda x: pd.Timestamp(x))
    germany_holidays['ds'] = germany_holidays['ds'].apply(lambda x: (x - timedelta(days=x.dayofweek+1)))
    return germany_holidays


def process_dataframe_and_make_split(df, cap=10000000, floor=10000, netto_percent_tv=0.15, cut_off_date='2020-03-01'):
    """ Function to create a test and training dataset based on a desired split date
    The TV spendings will also be converted vom brutto-> netto with an estimated ratio
    that can be set in the netto_percent_tv variable.
    columns for the sum of all spendings and sum of online spendings as well as the
    percentage of tv spendings from all and percentage of online spendings from all
    are added.
    helper columns for the distribution of youtube, online and search (in %) from the
    online spendings are added -> later used to keep the distribution the same after
    budget shift from TV to online

    input: df = Dataframe with all datapoints.
           cap = [default to 10.000.000] the maximum used when the
                 fbprophet model uses a logistic growth
           floor = [default to 10.000] the minimum used when the
                   fbprophet model uses a logistic growth
           netto_percent_tv = [default to 0.15 (15%)] used to convert
                              the TV spendings from Brutto to netto
           cut_off_date = [default to 2020-03-01] date used to make
                          test/training split

    output: df_training = training Dataframe with all new columns
            df_testing = testing Dataframe with all new columns
            df_all = entire Dataset with all new columns
            df_holidays = holidays dataframe for the desired time period
    """

    df_raw = df.copy()
    # Convert Woche to pandsa Timestamp and make new column with correct naming for fbprophet
    df_raw['ds'] = df_raw['Woche'].apply(lambda x: pd.Timestamp(x))
    # Assign cap and floor values to new columns
    df['cap'] = cap
    df['floor'] = floor
    #convert TV spendings to Netto
    df_raw['TV'] = df_raw['TV']*netto_percent_tv
    # Make new column with sum of all spendings
    df_raw['sum_all_spendings'] = df_raw['TV'] + df_raw['Online'] + df_raw['YouTube'] + df_raw['Search']
    # Make new column with sum of all online spendings
    df_raw['sum_all_online'] = df_raw['Online'] + df_raw['YouTube'] + df_raw['Search']
    # Make new columns with percentage of TV and Online from all spendings
    df_raw['perc_tv'] = df_raw['TV']/df_raw['sum_all_spendings']
    df_raw['perc_online'] = df_raw['sum_all_online']/df_raw['sum_all_spendings']
    # Make new columns with percentage of online, youtube and search  from all Online spendings
    df_raw['percent_online'] = df_raw['Online']/df_raw['sum_all_online']
    df_raw['percent_youtube'] = df_raw['YouTube']/df_raw['sum_all_online']
    df_raw['percent_search'] = df_raw['Search']/df_raw['sum_all_online']

    # make holidays dataframe for the given datestamps
    df_holidays = __make_holiday_df(df_raw)
    # make copy of Dataframe with the new columns to return
    df_all = df_raw.copy()
    # Define training set as everything that happened before cut_off_date
    df_training = df_raw[df_raw['ds']<pd.Timestamp(cut_off_date)]
    # Define testing set as everything that happened after cut_off_date
    df_testing = df_raw[df_raw['ds']>=pd.Timestamp(cut_off_date)]
    return df_training, df_testing, df_all, df_holidays


def build_shifted_testing_df_and_predict(model, df_testing, percent_keep=0.5):
    """ Function to build a dataframe and make a prediction for a given Budget shift.
    The original testing dataframe is getting modified taking the amount budget (in percent)
    to stay with TV into consideration.

    input: model = trained fbprophet model to use for prediction
           df_testing = original testing dataframe with original budget distribution
           percent_keep = percent of original value to stick with tv
                          (eg. to keep 75% with TV and distribute the
                          remaining 25% -> percent_keep=0.75)

    output: df_testing_shift = Dataframe with changed budget distribution
            y_pred = prediction dataframe for shifted budget distribution
            y_compare = two column Dataframe with datestamp and prediction for
                        later comparison of the different budget scenarios
    """

    df = df_testing.copy()
    if percent_keep == 0:
        y_compare_name = 'y_pred_00'
    else:
        y_compare_name = 'y_pred_{}'.format(int(percent_keep*100))

    df_testing_shift = distribute_budget_evenly(df, percent_keep)
    y_pred = model.predict(df_testing_shift)
    y_compare = helper.subset_and_rename(y_pred, {'ds':'ds', 'yhat':y_compare_name}).round(2)
    return df_testing_shift, y_pred, y_compare


def predict_and_evaluate(df_testing, model):
    """ Function to make a prediction on a given dataset with a given model
    returns both the evaluation dataframe containing different error measurements
    as well as individua error metrics (as an overall value)

    input: df_testing = Dataframe that is being used to predict and also evaluate
                        (necessary to have a given target value to compare to)
           model = fbprophet model used to make the prediction with

    output: evaluation_df = Dataframe with all evaluation metrics and the Predicted
                            and real target values
            mean_absolute_error = the average of the absolute error for the weekly prediction
            rmse = the root of the average of the absolute error squared for the weekly Prediction
            mape = the average of the percentage error of the weekly prediction
            monthly_mape = the average of the percentage error of the prediction when its
                           aggregated on a monthly level before making the percent error
                           calculation
    """

    X_test = df_testing.copy().drop(columns=['y'])
    y_test = df_testing[['ds','y']].rename(columns={'y':'y_real'})
    y_predict = model.predict(X_test)
    y_predict_eval = y_predict[['ds', 'yhat']].rename(columns={'yhat':'y_predict'})
    y_predict_eval['y_predict'] = y_predict_eval['y_predict'].apply(lambda x: round(x,2))
    evaluation_df = y_test.merge(y_predict_eval, on='ds')

    df_monthly = evaluation_df.copy()
    df_monthly['month'] = df_monthly['ds'].apply(lambda x: x + pd.DateOffset(day=1))
    df_monthly = df_monthly.groupby('month', as_index=False).sum()

    evaluation_df['abs_diff'] = abs(evaluation_df['y_real'] - evaluation_df['y_predict'])
    evaluation_df['abs_diff_squared'] = round(evaluation_df['abs_diff'] * evaluation_df['abs_diff'],2)
    evaluation_df['perc_diff'] = round((evaluation_df['abs_diff'])/((evaluation_df['y_real'] + evaluation_df['y_predict'])/2),2)
    evaluation_df['percent_error'] = abs(round(evaluation_df['y_real'] - evaluation_df['y_predict']) / evaluation_df['y_real'])
    mean_absolute_error = int(evaluation_df['abs_diff'].mean())
    rmse = int(math.sqrt(evaluation_df['abs_diff_squared'].mean()))
    mape = round(evaluation_df['percent_error'].mean(),4)

    df_monthly['abs_diff'] = abs(df_monthly['y_real'] - df_monthly['y_predict'])
    df_monthly['abs_diff_squared'] = round(df_monthly['abs_diff'] * df_monthly['abs_diff'],2)
    df_monthly['perc_diff'] = round((df_monthly['abs_diff'])/((df_monthly['y_real'] + df_monthly['y_predict'])/2),2)
    df_monthly['percent_error'] = abs(round(df_monthly['y_real'] - df_monthly['y_predict']) / df_monthly['y_real'])
    monthly_mean_absolute_error = int(df_monthly['abs_diff'].mean())
    monthly_rmse = int(math.sqrt(df_monthly['abs_diff_squared'].mean()))
    monthly_mape = round(df_monthly['percent_error'].mean(),4)

    return evaluation_df, mean_absolute_error, rmse, mape, monthly_mape


def see_components_of_prediction(df_testing, model):
    """ Function to see the impact of the different seasonality and regressor
    components of the model as a graph.

    input: df_testing = Dataframe that is being used to predict and also evaluate
                        (necessary to have a given target value to compare to)
           model = fbprophet model used to make the prediction with

    output: fig = set of figures for the impact of each seasonality component
                  that is being used in the prediction of the dataset.
    """

    X_test = df_testing.copy().drop(columns=['y'])
    y_test = df_testing[['ds','y']].rename(columns={'y':'y_real'})
    y_predict = model.predict(X_test)
    #fig = model.plot(y_predict)
    fig = model.plot_components(y_predict)
    return fig


def build_and_fit_fbprophet_model(df_training, df_holidays):
    """ Function to build and train model based on a given training set.
    Uses settings for fbprophet for seasonalities and regressors

    input: df_training = Dataframe that is being used to train the model with
           df_holidays = Dataframe containing the holidays used to fit the
                         models seasonality

    output: model = fitted model that can be used to predict any data
                    that contains all necessary columns
    """
    # Building fpbrophet construct
    model = fbprophet.Prophet(
                                growth='logistic',
                                # manual adding changepoints
                                #changepoints= maybe add corona as list of changepoints
                                n_changepoints=10,
                                #increase for more flexibility
                                changepoint_prior_scale=0.4,
                                # Fouriers Order for seasonalities
                                yearly_seasonality=False,
                                weekly_seasonality=False,
                                daily_seasonality=False,
                                holidays=df_holidays,
                                seasonality_mode='additive',
                                holidays_prior_scale=0.5,
                                interval_width=0.8
                            )
    # add monthly seasonality
    model.add_seasonality(name='yearly', period=365, fourier_order=20, prior_scale=0.33)
    model.add_seasonality(name='monthly', period=52/12, fourier_order=5, prior_scale=0.4)
    model.add_regressor('tv_spendings', prior_scale=0.032, mode='multiplicative')

    model.add_regressor('perc_tv', prior_scale=0.02, mode='additive')
    model.add_regressor('perc_online', prior_scale=0.02, mode='additive')
    model.add_regressor('sum_all_online', prior_scale=0.02, mode='additive')

    # model.add_regressor('online_spendings', prior_scale=0.6, mode='additive')
    # model.add_regressor('youtube_spendings', prior_scale=0.6, mode='additive')
    # model.add_regressor('search_spendings', prior_scale=0.6, mode='additive')
    #model.add_regressor('google_trend_index', prior_scale=0.02, mode='additive')
    model.fit(df_training)
    return model


def create_figure_revenue_develop_with_shift(y_pred_all_scenarios):
    """ Function to create a graph showing the revenue development for all different shifts

    input: y_pred_all_scenarios = Dataframe containing all different prediction scenarios

    output: fig = plotly figure with the development per scenario as a line graph
    """
    fig = go.Figure()
    list_columns = list(y_pred_all_scenarios.columns)
    if 'ds' in list_columns: list_columns.remove('ds')
    #column_name = 'y_pred_100'
    i = 0
    colors = ['#00FFCF', '#219C87', '#217363', '#215049', '#103A33']
    trace_list =[]
    for column_name in list_columns:

        if 'y_real' in column_name:
            fig.add_trace(go.Scatter(x=y_pred_all_scenarios['ds'], y=y_pred_all_scenarios['y_real'],
                                name='Tatsächlicher Umsatz',
                                line=dict(color='#FF0230', width=3)
                                ))
        if 'y_pred' in column_name:
            color = colors[i]
            i = i + 1
            title_number = re.findall('\d+', column_name)[0]
            trace_list.append(go.Scatter(x=y_pred_all_scenarios['ds'], y=y_pred_all_scenarios[column_name],
                                name='TV {}% & {}% Shift'.format(title_number, 100-int(title_number)),
                                line=dict(color=color, width=3, dash='dot')
                                ))

    fig.add_traces(trace_list)
    fig.update_layout(title='Umsatzentwicklung für TV-Budget Shifts',
                       xaxis=dict(
                            title_text='Datum',
                            title_font = {"size": 20},
                        ticks='outside',
                        tickfont=dict(
                            family='Montserrat',
                            size=12,
                            color='black',
                        ),),
                        yaxis=dict(
                            title_text='Umsatz [€]',
                            #range=[-40,10],
                            title_font = {"size": 20},
                        ticks='outside',
                        tickfont=dict(
                            family='Montserrat',
                            size=12,
                            color='black',
                        ),),
                       #yaxis_title='percent diff [%])',
                       plot_bgcolor="#F2F2F2",
                       width=1100,
                       height=600,
                       font=dict(
                            family="Montserrat",
                            size=20,
                            color="black"
                            )
                        )
    return fig


def create_figure_revenue_percent_diff(y_pred_all_scenarios):
    """ Function to create a graph showing the revenue difference for all different shifts

    input: y_pred_all_scenarios = Dataframe containing all different prediction scenarios

    output: fig = plotly figure with the percent difference per scenario as a line graph
    """
    fig = go.Figure()
    list_columns = list(y_pred_all_scenarios.columns)
    if 'ds' in list_columns: list_columns.remove('ds')
    #column_name = 'y_pred_100'
    i = 0
    colors = ['#00FFCF', '#219C87', '#217363', '#215049', '#103A33']
    trace_list =[]
    for column_name in list_columns:

        if 'revenue_drop' in column_name:
            color = colors[i]
            i = i + 1
            title_number = re.findall('\d+', column_name)[0]
            trace_list.append(go.Scatter(x=y_pred_all_scenarios['ds'], y=y_pred_all_scenarios[column_name],
                                name='TV {}% & {}% Shift'.format(title_number, 100-int(title_number)),
                                line=dict(color=color, width=3, dash='dot')
                                ))

    fig.add_traces(trace_list)
    fig.update_layout(title='Umsatzdifferenz bei TV-Budget Shifts in Prozent',
                       xaxis=dict(
                            title_text='Datum',
                            title_font = {"size": 20},
                        ticks='outside',
                        tickfont=dict(
                            family='Montserrat',
                            size=12,
                            color='black',
                        ),),
                        yaxis=dict(
                            title_text='Differenz in Prozent [%]',
                            range=[-40,10],
                            title_font = {"size": 20},
                        ticks='outside',
                        tickfont=dict(
                            family='Montserrat',
                            size=12,
                            color='black',
                        ),),
                       #yaxis_title='percent diff [%])',
                       plot_bgcolor="#F2F2F2",
                       width=1100,
                       height=600,
                       font=dict(
                            family="Montserrat",
                            size=20,
                            color="black"
                            )
                        )
    return fig
