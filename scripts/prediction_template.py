# importing general models
import os
import sys

# importing desired packages for python
import pandas as pd
import numpy as np
import warnings
from datetime import timedelta, datetime
import holidays
import matplotlib.pyplot as plt
import fbprophet
import math
import plotly.graph_objects as go

# global settings
CWD = os.path.abspath('.')
sys.path.append(CWD + '\\scripts')

try:
    import helper_functions as helper
except:
    print('there was an error importing the submodule')


# display settings for output
pd.options.display.max_columns = 50
pd.options.display.max_rows = 2000

# Filter the warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
df = """INSERT LOCATION OF CSV HERE """
cap = df.Umsatz.max()*1.1
floor=100000

#df = pd.read_csv(CWD + '/data/loreal_raw.csv')
# Create testing and training dataframe using 2020-03-01 as the split date
df_training, df_testing, df_all, df_holidays = helper.process_dataframe_and_make_split(df, cap=cap, floor=100000, netto_percent_tv=0.15, cut_off_date='2020-03-01')

columns_to_keep_testing = {
                            'ds':'ds',
                            'Umsatz': 'y',
                            'cap':'cap',
                            'floor':'florr',
                            'TV' :'tv_spendings',
                            'Online': 'online_spendings',
                            'YouTube': 'youtube_spendings',
                            'Search': 'search_spendings',
                            'Google_Trend_Index': 'google_trend_index',
                            'perc_tv':'perc_tv',
                            'perc_online':'perc_online',
                            'sum_all_online':'sum_all_online',
                            'percent_online': 'percent_online',
                            'percent_youtube': 'percent_youtube',
                            'percent_search': 'percent_search',
                            'sum_all_spendings': 'sum_all_spendings'
                        }
columns_to_keep_training = {
                            'ds':'ds',
                            'Umsatz': 'y',
                            'cap':'cap',
                            'floor':'florr',
                            'TV' :'tv_spendings',
                            'Online': 'online_spendings',
                            'YouTube': 'youtube_spendings',
                            'Search': 'search_spendings',
                            'Google_Trend_Index': 'google_trend_index',
                            'perc_tv':'perc_tv',
                            'perc_online':'perc_online',
                            'sum_all_online':'sum_all_online'
                        }
df_training = helper.subset_and_rename(df_training, columns_to_keep_training)
df_testing = helper.subset_and_rename(df_testing, columns_to_keep_testing)
# Build a list with TV shifts you want to see
# Where 1 = 100% TV & 0% shift ; 0.5 = 50% TV & 50% shift to online ; 0 = 0% TV & 100% shift to online


model = helper.build_and_fit_fbprophet_model(df_training, df_holidays)

eval_df, mae, rmse, mape, monthly_mape = helper.predict_and_evaluate(df_testing, model)
print('the mape for the original budget allocation is {}'.format(mape))
print('the monhtly aggregate mape for the original budget allocation is {}'.format(monthly_mape))


list_with_tv_shift_options = [1,0.75,0.5,0.25,0]
list_for_y_compare = []
list_for_testing_dfs = []
list_for_y_predict = []
# create testing sets with different budget allocation


df_testing_100, y_pred_100, y_compare_100 =  helper.build_shifted_testing_df_and_predict(model, df_testing, percent_keep=1)

df_testing_75, y_pred_75, y_compare_75 =  build_shifted_testing_df_and_predict(model, df_testing, percent_keep=0.75)
df_testing_50, y_pred_50, y_compare_50 =  build_shifted_testing_df_and_predict(model, df_testing, percent_keep=0.50)
df_testing_25, y_pred_25, y_compare_25 =  build_shifted_testing_df_and_predict(model, df_testing, percent_keep=0.25)
df_testing_00, y_pred_00, y_compare_00 =  build_shifted_testing_df_and_predict(model, df_testing, percent_keep=0)


y_compare_real = helper.subset_and_rename(df_testing,{'ds':'ds','y':'y_real'}).round(2)

y_pred_all_scenarios = y_compare_real.merge(y_compare_100).merge(y_compare_75).merge(y_compare_50).merge(y_compare_25).merge(y_compare_00)

y_pred_all_scenarios['%_revenue_drop_100'] = 0*(y_pred_all_scenarios['y_pred_100'])
y_pred_all_scenarios['%_revenue_drop_75'] = 100*(-y_pred_all_scenarios['y_pred_100']+y_pred_all_scenarios['y_pred_75'])/y_pred_all_scenarios['y_pred_100']
y_pred_all_scenarios['%_revenue_drop_50'] = 100*(-y_pred_all_scenarios['y_pred_100']+y_pred_all_scenarios['y_pred_50'])/y_pred_all_scenarios['y_pred_100']
y_pred_all_scenarios['%_revenue_drop_25'] = 100*(-y_pred_all_scenarios['y_pred_100']+y_pred_all_scenarios['y_pred_25'])/y_pred_all_scenarios['y_pred_100']
y_pred_all_scenarios['%_revenue_drop_00'] = 100*(-y_pred_all_scenarios['y_pred_100']+y_pred_all_scenarios['y_pred_00'])/y_pred_all_scenarios['y_pred_100']

fig_development = helper.create_figure_revenue_develop_with_shift(y_pred_all_scenarios)
fig_comparison = helper.create_figure_revenue_percent_diff(y_pred_all_scenarios)
